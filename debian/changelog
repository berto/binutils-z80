binutils-z80 (5) unstable; urgency=medium

  * Set the debhelper compatibility level to 10:
    - Get rid of debian/compat.
    - Add build dependency on debhelper-compat.
    - Don't pass --parallel to dh in debian/rules.
  * Update the lintian overrides.
  * debian/control:
    - Set Rules-Requires-Root: no.
    - Change priority from extra to optional.
    - Update Standards-Version to 4.7.0.
    - Add Vcs-Browser and Vcs-Git fields.
  * debian/copyright:
    - Use HTTPS for all URLs.
    - Update copyright years.
  * debian/rules:
    - Change build dir to 'build-dir' to avoid conflicts with the 'build'
      rule.
    - Install ldscripts in /usr/lib/z80-unknown-coff (Closes: #1092277).

 -- Alberto Garcia <berto@igalia.com>  Tue, 07 Jan 2025 19:46:52 +0100

binutils-z80 (4) unstable; urgency=medium

  * Don't patch tz-z80.c anymore, all changes have been merged upstream.
    (Closes: #746127).
    - debian/tc-z80.c.diff: drop the patch.
    - debian/control: update build-dependency on binutils-source to
      2.24.51.20140411.
    - debian/rules: remove all references to the patch.
  * debian/copyright: update copyright years.
  * debian/control: update Standards-Version to 3.9.5 (no changes).
  * debian/rules: allow parallel builds.

 -- Alberto Garcia <berto@igalia.com>  Sun, 27 Apr 2014 23:35:39 +0300

binutils-z80 (3) unstable; urgency=low

  * Build using binutils-source >= 2.23.52 (Closes: #712315).
  * Turn binutils-z80 into a native package. The source tarball was
    unnecessary because it simply contained a symlink to the actual
    source code.
    - Remove the debian/watch file.
    - debian/rules: get the version number for the binary package from
      binutils-source.
    - debian/control: don't restrict the build dependency on
      binutils-source to a particular version of that package.
    - debian/copyright: remove mentions to the binutils-z80 source
      tarball.
    - debian/source/format: set to 3.0 (native).
  * Upload to unstable.
  * Update my e-mail address in debian/control and debian/copyright.
  * debian/rules: install the changelog from the upstream tarball.
  * debian/control: update Standards-Version to 3.9.4 (no changes).
  * debian/README.Debian: fix typos.

 -- Alberto Garcia <berto@igalia.com>  Sun, 16 Jun 2013 16:24:05 +0300

binutils-z80 (2.23.1-1) experimental; urgency=low

  * New upstream release.
  * Depend on binutils 2.23.1:
    - debian/control: update Build-Depends.
    - debian/rules: update name of tarball.
  * Properly implement the Built-Using field:
    - The correct name is Built-Using, not Build-Using.
    - Move it to the binary paragraph.
    - Generate its value using dh_gencontrol in debian/rules.

 -- Alberto Garcia <agarcia@igalia.com>  Mon, 19 Nov 2012 10:29:54 +0200

binutils-z80 (2.23-1) experimental; urgency=low

  * New upstream release.
  * Depend on binutils 2.23:
    - debian/control: update Build-Depends and Build-Using fields.
    - debian/rules: update name of tarball.
  * Simplify build script using the debhelper sequencer:
    - debian/compat: set compatibility level to 9.
    - debian/control: build depend on debhelper >= 9.
    - debian/rules: rewrite using dh 9.
    - debian/install: select the files that we want, instead of excluding
      the ones that we don't want.
  * debian/control: remove the DM-Upload-Allowed field, it's now obsolete.
  * debian/control: remove all redundant dependencies.
  * debian/copyright: update copyright years.
  * debian/app.c.diff: drop patch, it's already included in this release.
  * debian/tc-z80.c.diff: update to fix two new compilation errors.

 -- Alberto Garcia <agarcia@igalia.com>  Tue, 13 Nov 2012 10:45:00 +0200

binutils-z80 (2.22-3) unstable; urgency=low

  * debian/app.c.diff: fix FTBFS (array subscript is below array bounds)
    (Closes: #672057).
  * debian/binutils-z80.lintian-overrides: override lintian warnings about
    missing manual pages.
  * Remove unused debian/patches directory.

 -- Alberto Garcia <agarcia@igalia.com>  Mon, 14 May 2012 00:30:22 +0300

binutils-z80 (2.22-2) unstable; urgency=low

  * debian/rules: make sure configure is called in the build-arch target
    (Closes: #666355).
  * debian/copyright: update Debian copyright format URL.
  * debian/control: update Standards-Version to 3.9.3.
  * debian/control: add Multi-Arch foreign header.
  * debian/control: add DM-Upload-Allowed flag.

 -- Alberto Garcia <agarcia@igalia.com>  Fri, 30 Mar 2012 14:09:40 +0300

binutils-z80 (2.22-1) unstable; urgency=low

  * Update to binutils 2.22 (Closes: #639590):
    - debian/rules: change version of tar.gz file.
    - debian/control: update build dependency.
  * Take over maintainership (Closes: #634060, #652451):
    - debian/control: update Maintainer field.
  * debian/control: update Standards-Version to 3.9.2.
  * debian/control: remove redundant fields from the binary paragraph.
  * debian/control: fix 'description-synopsis-starts-with-article'.
  * debian/rules: add build-indep and build-arch targets.
  * debian/rules: patch tc-z80.c instead of shipping an entire copy of the
    file.
  * debian/watch: new file.
  * debian/copyright: rewrite using the machine-readable format.

 -- Alberto Garcia <agarcia@igalia.com>  Sat, 24 Dec 2011 03:39:10 +0100

binutils-z80 (2.21.52-2) unstable; urgency=low

  * temp fix build by overriding tc-z80.c from the binutils tarfile
    (don't forget to undo that on the next binutils version!)
  * Orphan the package.

 -- Joost Yervante Damad <andete@debian.org>  Sat, 16 Jul 2011 15:36:26 +0200

binutils-z80 (2.21.52-1) unstable; urgency=low

  * don't install libs and headers (Closes: #621273)
  * update for binutils 2.21.51 (Closes: #628506, #629755)

 -- Joost Yervante Damad <andete@debian.org>  Fri, 10 Jun 2011 16:54:04 +0200

binutils-z80 (2.21.0-1) unstable; urgency=low

  * updated to binutils 2.21.0.20110322 (Closes: #618656, #613180)
  * introduce Build-Using debian/control flag
  * adapt to use xz tarfile

 -- Joost Yervante Damad <andete@debian.org>  Sun, 27 Mar 2011 12:28:11 +0200

binutils-z80 (2.20.1-1) unstable; urgency=low

  * update to binutils 2.20.1 (Closes: #573668)

 -- Joost Yervante Damad <andete@debian.org>  Tue, 13 Apr 2010 08:25:07 +0200

binutils-z80 (2.20-3) unstable; urgency=low

  * update standards version to 3.8.3
  * remove traces of dpatch
  * clean up rules

 -- Joost Yervante Damad <andete@debian.org>  Sat, 19 Dec 2009 22:36:48 +0100

binutils-z80 (2.20-2) unstable; urgency=low

  * update source format to 3.0

 -- Joost Yervante Damad <andete@debian.org>  Sat, 19 Dec 2009 21:10:32 +0100

binutils-z80 (2.20-1) unstable; urgency=low

  * update to binutils 2.20 (Closes: #552960)

 -- Joost Yervante Damad <andete@debian.org>  Thu, 29 Oct 2009 19:38:58 +0100

binutils-z80 (2.19.91-1) unstable; urgency=low

  * update to binutils 2.19.91 (Closes: #547618)

 -- Joost Yervante Damad <andete@debian.org>  Sat, 10 Oct 2009 13:39:15 +0200

binutils-z80 (2.19.51.20090827-2) unstable; urgency=low

  * don't include manpages (use binutils-doc for that!)
    (Closes: #545767)
  * don't include info pages as well

 -- Joost Yervante Damad <andete@debian.org>  Wed, 09 Sep 2009 07:41:10 +0200

binutils-z80 (2.19.51.20090827-1) unstable; urgency=low

  * update to binutils 2.19.51.20090827
  * no longer needed to mv manpages (Closes: #545664)
    (thanks to Lucas Nussbaum for doing full rebuilds of the archive!)

 -- Joost Yervante Damad <andete@debian.org>  Tue, 08 Sep 2009 19:16:55 +0200

binutils-z80 (2.19.51.20090704-1) unstable; urgency=low

  * update to binutils 2.19.51.20090704 (Closes: #536990)

 -- Joost Yervante Damad <andete@debian.org>  Fri, 17 Jul 2009 07:26:27 +0200

binutils-z80 (2.19.1-1) unstable; urgency=low

  * update to 2.19.1
  * lintian fix: build-depends-on-1-revision
  * make buildable (closes: #490748)

  [from ubuntu:]
    * Use binutils-2.18.50.tar.bz2 instead of binutils-2.18.0.tar.bz2, there is
      a new version of binutils in Ubuntu, fix FTBFS (LP: #253374).
    * Add dpatch support.
    * Add fix_format_strings.dpatch patch, fix FTBFS.
    -- Devid Filoni <d.filoni@techemail.com>  Wed, 30 Jul 2008 18:37:01 +0200

 -- Joost Yervante Damad <andete@debian.org>  Sat, 09 May 2009 16:12:05 +0200

binutils-z80 (2.18.0-1) unstable; urgency=low

  * use version 2.18.0 instead of 2.18 for soft-link

 -- Joost Yervante Damad <andete@debian.org>  Tue, 08 Jan 2008 20:41:35 +0100

binutils-z80 (2.18-1) unstable; urgency=low

  * Initial release (closes: #458446)
  * removed package dependency on dbs, clean up debian/rules
  * package initially based on binutils-avr

 -- Joost Yervante Damad <andete@debian.org>  Mon, 31 Dec 2007 13:47:34 +0100
